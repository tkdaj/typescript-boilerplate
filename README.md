# Getting Started 

A template to build TypeScript and vanilla JS files which
contains boilerplate for setting up a project with:

- strict linting
- prettier configuration
- typescript
- webpack
- ability to import files with the following extensions:
  ( js, mjs, ts, json, css, sass, bmp, gif, jpg, jpeg, png ) 
  
and it uses webpack file-loader to import anything else

If you want webpack files to be served from anywhere other than domain/ locally, you'll want to change the "homepage" variable within package.json
